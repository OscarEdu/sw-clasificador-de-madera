namespace SEB_Buzones.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Carro")]
    public partial class Carro
    {
        [Key]
        public int Carro_id { get; set; }

        public int? Calidad_id { get; set; }

        public int? Buzon_id { get; set; }

        public int? Carro_byte_corte { get; set; }

        public virtual Buzon Buzon { get; set; }

        public virtual Calidad Calidad { get; set; }
    }
}
