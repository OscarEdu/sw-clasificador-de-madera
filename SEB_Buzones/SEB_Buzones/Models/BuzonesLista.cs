using System;
using System.IO;

namespace SEB_Buzones.Models
{
    public class BuzonesLista
    {
        public int Buzon_id;
        public int Escuadrias_id;
        public int Buzon_total;
        public int Buzon_cantidad;
        public string Buzon_estado;
        public string Escuadrias_nombre;
        public int Escuadrias_cantidad_tablas;
        public Double Escuadrias_espesor;
        public Double Escuadrias_ancho;
        public Double Escuadrias_largo;
    }

    public class EscuadriasLista
    {
        
        public int Escuadrias_id;
        public Double Escuadrias_espesor;
        public Double Escuadrias_ancho;
        public Double Escuadrias_largo;
        public int Escuadrias_cantidad_tablas;
    }
}