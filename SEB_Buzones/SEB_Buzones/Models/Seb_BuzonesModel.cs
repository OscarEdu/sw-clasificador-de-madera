using System.Web.Helpers;
using Microsoft.Ajax.Utilities;

namespace SEB_Buzones.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Configuration;
    using System.Data;

    public partial class Seb_BuzonesModel : DbContext
    {
        public Seb_BuzonesModel()
            : base("name=Seb_BuzonesModel")
        {
        }

        public virtual DbSet<Buzon> Buzons { get; set; }
        public virtual DbSet<Calidad> Calidads { get; set; }
        public virtual DbSet<Carro> Carroes { get; set; }
        public virtual DbSet<Escuadria> Escuadrias { get; set; }
        public virtual DbSet<Tabla> Tablas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Buzon>()
                .Property(e => e.Buzon_estado)
                .IsUnicode(false);

            modelBuilder.Entity<Calidad>()
                .Property(e => e.Calidad_marca)
                .IsUnicode(false);

            modelBuilder.Entity<Calidad>()
                .Property(e => e.Calidad_descripcion)
                .IsUnicode(false);
        }
        string cs = ConfigurationManager.ConnectionStrings["Seb_BuzonesModel"].ConnectionString;
        public List<Escuadria> ListAll()
        {
            List<Escuadria> lst = new List<Escuadria>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"SELECT * FROM Escuadrias";
                SqlCommand com = new SqlCommand(query, con);
                
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lst.Add(new Escuadria
                    {
                        Escuadrias_nombre = Convert.ToString(rdr["Escuadrias_nombre"] is DBNull ? 0: rdr["Escuadrias_nombre"]),
                        Escuadrias_id = Convert.ToInt32(rdr["Escuadrias_id"] is DBNull ? 0: rdr["Escuadrias_id"]),
                        Calidad_id = Convert.ToInt32(rdr["Calidad_id"] is DBNull ? 0 : rdr["Calidad_id"]),
                        Escuadrias_espesor = Convert.ToDouble(rdr["Escuadrias_espesor"] is DBNull ? 0 : rdr["Escuadrias_espesor"]),
                        Escuadrias_ancho = Convert.ToDouble(rdr["Escuadrias_ancho"] is DBNull ? 0 : rdr["Escuadrias_ancho"]),
                        Escuadrias_largo = Convert.ToDouble(rdr["Escuadrias_largo"] is DBNull ? 0 : rdr["Escuadrias_largo"]),
                        Escuadrias_porcentaje_participacion = Convert.ToInt32(rdr["Escuadrias_porcentaje_participacion"] is DBNull ? 0 : rdr["Escuadrias_porcentaje_participacion"]),
                        Escuadrias_porcentaje_espesor = Convert.ToInt32(rdr["Escuadrias_porcentaje_espesor"] is DBNull ? 0 : rdr["Escuadrias_porcentaje_espesor"]),
                        Escuadrias_porcentaje_ancho = Convert.ToInt32(rdr["Escuadrias_porcentaje_ancho"] is DBNull ? 0 : rdr["Escuadrias_porcentaje_ancho"]),
                        Escuadrias_porcentaje_largo = Convert.ToInt32(rdr["Escuadrias_porcentaje_largo"] is DBNull ? 0 : rdr["Escuadrias_porcentaje_largo"]),
                        Escuadrias_cantidad_tablas = Convert.ToInt32(rdr["Escuadrias_cantidad_tablas"] is DBNull ? 0 : rdr["Escuadrias_cantidad_tablas"]),
                    });
                }
                return lst;
            }

        }
        
        public List<BuzonesLista> ListBuzons()
        {
                List<BuzonesLista> lista = new List<BuzonesLista>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"SELECT Buzon.Buzon_id, Buzon.Buzon_cantidad, Buzon.Buzon_total, Buzon.Buzon_estado, Buzon.Escuadrias_id,
                                Escuadrias.Escuadrias_nombre, Escuadrias.Escuadrias_cantidad_tablas, Escuadrias.Escuadrias_ancho, Escuadrias.Escuadrias_largo, Escuadrias.Escuadrias_espesor
                                FROM Buzon
                                INNER JOIN Escuadrias ON Buzon.Escuadrias_id = Escuadrias.Escuadrias_id";
                SqlCommand com = new SqlCommand(query, con);
                
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lista.Add(new BuzonesLista
                    {
                        Buzon_id = Convert.ToInt32(rdr["Buzon_id"] is DBNull ? 0: rdr["Buzon_id"]),
                        Escuadrias_id = Convert.ToInt32(rdr["Escuadrias_id"] is DBNull ? 0: rdr["Escuadrias_id"]),
                        Buzon_total = Convert.ToInt32(rdr["Buzon_total"] is DBNull ? 0 : rdr["Buzon_total"]),
                        Buzon_estado = Convert.ToString(rdr["Buzon_estado"] is DBNull ? 0 : rdr["Buzon_estado"]),
                        Buzon_cantidad = Convert.ToInt32(rdr["Buzon_cantidad"] is DBNull ? 0 : rdr["Buzon_cantidad"]),
                        Escuadrias_nombre = Convert.ToString(rdr["Escuadrias_nombre"] is DBNull ? 0 : rdr["Escuadrias_nombre"]),
                        Escuadrias_cantidad_tablas = Convert.ToInt32(rdr["Escuadrias_cantidad_tablas"] is DBNull ? 0 : rdr["Escuadrias_cantidad_tablas"]),
                        Escuadrias_ancho = Convert.ToDouble(rdr["Escuadrias_ancho"] is DBNull ? 0 : rdr["Escuadrias_ancho"]),
                        Escuadrias_largo = Convert.ToDouble(rdr["Escuadrias_largo"] is DBNull ? 0 : rdr["Escuadrias_largo"]),
                        Escuadrias_espesor = Convert.ToDouble(rdr["Escuadrias_espesor"] is DBNull ? 0 : rdr["Escuadrias_espesor"]),

                    });
                }
                return lista;
            }

        }
        
        public List<EscuadriasLista> ListEscuadrias()
        {
                List<EscuadriasLista> lista = new List<EscuadriasLista>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"SELECT * FROM Escuadrias";
                SqlCommand com = new SqlCommand(query, con);
                
                SqlDataReader rdr = com.ExecuteReader();
                while (rdr.Read())
                {
                    lista.Add(new EscuadriasLista
                    {
                        Escuadrias_id = Convert.ToInt32(rdr["Escuadrias_id"] is DBNull ? 0: rdr["Escuadrias_id"]),
                        Escuadrias_cantidad_tablas = Convert.ToInt32(rdr["Escuadrias_cantidad_tablas"] is DBNull ? 0 : rdr["Escuadrias_cantidad_tablas"]),
                        Escuadrias_ancho = Convert.ToDouble(rdr["Escuadrias_ancho"] is DBNull ? 0 : rdr["Escuadrias_ancho"]),
                        Escuadrias_largo = Convert.ToDouble(rdr["Escuadrias_largo"] is DBNull ? 0 : rdr["Escuadrias_largo"]),
                        Escuadrias_espesor = Convert.ToDouble(rdr["Escuadrias_espesor"] is DBNull ? 0 : rdr["Escuadrias_espesor"]),
                    });
                }
                return lista;
            }

        }

        public int Add(Escuadria escuadria)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"INSERT INTO Escuadrias(
                                    Calidad_id,
                                    Escuadrias_nombre,
                                    Escuadrias_espesor,
                                    Escuadrias_ancho,
                                    Escuadrias_largo,
                                    Escuadrias_porcentaje_participacion,
                                    Escuadrias_porcentaje_espesor,
                                    Escuadrias_porcentaje_ancho,
                                    Escuadrias_porcentaje_largo,
                                    Escuadrias_cantidad_tablas)
                                values(@Calidad_id, @Escuadrias_nombre, @Escuadrias_espesor, @Escuadrias_ancho, @Escuadrias_largo,
                                        @Escuadrias_porcentaje_participacion, @Escuadrias_porcentaje_espesor,
                                        @Escuadrias_porcentaje_ancho, @Escuadrias_porcentaje_largo,
                                        @Escuadrias_cantidad_tablas)";
                SqlCommand com = new SqlCommand(query, con);
               
                com.Parameters.AddWithValue("@Escuadrias_nombre", escuadria.Escuadrias_nombre);
                com.Parameters.AddWithValue("@Calidad_id", escuadria.Calidad_id);
                com.Parameters.AddWithValue("@Escuadrias_espesor", escuadria.Escuadrias_espesor);
                com.Parameters.AddWithValue("@Escuadrias_ancho", escuadria.Escuadrias_ancho);
                com.Parameters.AddWithValue("@Escuadrias_largo", escuadria.Escuadrias_largo);
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_participacion", escuadria.Escuadrias_porcentaje_participacion);
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_espesor", escuadria.Escuadrias_porcentaje_espesor);
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_ancho", escuadria.Escuadrias_porcentaje_ancho);
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_largo", escuadria.Escuadrias_porcentaje_largo);
                com.Parameters.AddWithValue("@Escuadrias_cantidad_tablas", escuadria.Escuadrias_cantidad_tablas);
                i = com.ExecuteNonQuery();
            }
            return i;
        }

        public int Update(Escuadria escuadria)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string query = @"UPDATE Escuadrias SET
                                    Calidad_id = @Calidad_id,
                                    Escuadrias_nombre = @Escuadrias_nombre,
                                    Escuadrias_espesor = @Escuadrias_espesor,
                                    Escuadrias_ancho = @Escuadrias_ancho,
                                    Escuadrias_largo = @Escuadrias_largo,
                                    Escuadrias_porcentaje_participacion =@Escuadrias_porcentaje_participacion,
                                    Escuadrias_porcentaje_espesor =@Escuadrias_porcentaje_espesor,
                                    Escuadrias_porcentaje_ancho =@Escuadrias_porcentaje_ancho,
                                    Escuadrias_porcentaje_largo =@Escuadrias_porcentaje_largo,
                                    Escuadrias_cantidad_tablas =@Escuadrias_cantidad_tablas
                                    WHERE Escuadrias_id = @Escuadrias_id";
                SqlCommand com = new SqlCommand(query, con);
                
                com.Parameters.AddWithValue("@Escuadrias_id", escuadria.Escuadrias_id);
                com.Parameters.AddWithValue("@Escuadrias_nombre", escuadria.Escuadrias_nombre);
                com.Parameters.AddWithValue("@Calidad_id", escuadria.Calidad_id);
                com.Parameters.AddWithValue("@Escuadrias_espesor", Convert.ToDecimal(escuadria.Escuadrias_espesor));
                com.Parameters.AddWithValue("@Escuadrias_ancho", Convert.ToDecimal(escuadria.Escuadrias_ancho));
                com.Parameters.AddWithValue("@Escuadrias_largo", Convert.ToDecimal(escuadria.Escuadrias_largo));
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_participacion", escuadria.Escuadrias_porcentaje_participacion);
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_espesor", escuadria.Escuadrias_porcentaje_espesor);
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_ancho", escuadria.Escuadrias_porcentaje_ancho);
                com.Parameters.AddWithValue("@Escuadrias_porcentaje_largo", escuadria.Escuadrias_porcentaje_largo);
                com.Parameters.AddWithValue("@Escuadrias_cantidad_tablas", escuadria.Escuadrias_cantidad_tablas);

                try
                {
                    i = Convert.ToInt32(com.ExecuteNonQuery());
                    
                }
                catch (Exception ex)
                {
                    i = 1;
                }
                
            }
            return i;
        }

        public int UpdateBuzon(List<Buzon> buzon)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                
                foreach (var item in buzon)
                {
                    string query = @"UPDATE Buzon SET
                                    Escuadrias_id = @Escuadrias_id
                                    WHERE Buzon_id = @Buzon_id";
                    SqlCommand com = new SqlCommand(query, con);
                    com.Parameters.AddWithValue("@Escuadrias_id", item.Escuadrias_id);
                    com.Parameters.AddWithValue("@Buzon_id", item.Buzon_id);
                    com.ExecuteNonQuery();
                }
                    
                    
                    
                try
                {
                    i = 200;
                    
                }
                catch (Exception ex)
                {
                    i = 1;
                }
                 
               
            }
            return i;
        }

        public int Delete(int ID)
        {
            int i;
            using (SqlConnection con = new SqlConnection(cs))
            {
                con.Open();
                string delete = @"DELETE Escuadrias where Escuadrias_id = @Escuadrias_id";
                SqlCommand com = new SqlCommand(delete, con);
                
                com.Parameters.AddWithValue("@Escuadrias_id", ID);
                i = com.ExecuteNonQuery();
            }
            return i;
        }
    }
}
