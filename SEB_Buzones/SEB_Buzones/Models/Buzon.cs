namespace SEB_Buzones.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Buzon")]
    public partial class Buzon
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Buzon()
        {
            Carroes = new HashSet<Carro>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Buzon_id { get; set; }

        public int? Buzon_cantidad { get; set; }

        public int? Buzon_total { get; set; }

        [StringLength(10)]
        public string Buzon_estado { get; set; }

        public int? Escuadrias_id { get; set; }

        public virtual Escuadria Escuadria { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Carro> Carroes { get; set; }
    }
}
