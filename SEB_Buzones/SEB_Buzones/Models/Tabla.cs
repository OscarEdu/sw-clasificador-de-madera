namespace SEB_Buzones.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tabla")]
    public partial class Tabla
    {
        [Key]
        public int Tabla_id { get; set; }

        public int? Calidad_id { get; set; }

        public double? Tabla_espesor { get; set; }

        public double? Tabla_ancho { get; set; }

        public double? Tabla_largo_inicial { get; set; }

        public double? Tabla_largo_final { get; set; }

        public DateTime? Tabla_fecha_hora { get; set; }

        public virtual Calidad Calidad { get; set; }
    }
}
