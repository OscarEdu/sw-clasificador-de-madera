namespace SEB_Buzones.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Escuadria
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Escuadria()
        {
            Buzons = new HashSet<Buzon>();
        }

        [Key]
        public int Escuadrias_id { get; set; }
        public string Escuadrias_nombre { get; set; }

        public int? Calidad_id { get; set; }

        public double? Escuadrias_espesor { get; set; }

        public double? Escuadrias_ancho { get; set; }

        public double? Escuadrias_largo { get; set; }

        public int? Escuadrias_porcentaje_participacion { get; set; }

        public int? Escuadrias_porcentaje_espesor { get; set; }

        public int? Escuadrias_porcentaje_ancho { get; set; }

        public int? Escuadrias_porcentaje_largo { get; set; }

        public int? Escuadrias_cantidad_tablas { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Buzon> Buzons { get; set; }

        public virtual Calidad Calidad { get; set; }
    }
}
