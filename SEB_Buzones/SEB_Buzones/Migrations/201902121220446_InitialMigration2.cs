namespace SEB_Buzones.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Buzon",
                c => new
                    {
                        Buzon_id = c.Int(nullable: false),
                        Buzon_cantidad = c.Int(),
                        Buzon_total = c.Int(),
                        Buzon_estado = c.String(maxLength: 10, unicode: false),
                        Escuadrias_id = c.Int(),
                    })
                .PrimaryKey(t => t.Buzon_id)
                .ForeignKey("dbo.Escuadrias", t => t.Escuadrias_id)
                .Index(t => t.Escuadrias_id);
            
            CreateTable(
                "dbo.Carro",
                c => new
                    {
                        Carro_id = c.Int(nullable: false, identity: true),
                        Calidad_id = c.Int(),
                        Buzon_id = c.Int(),
                        Carro_byte_corte = c.Int(),
                    })
                .PrimaryKey(t => t.Carro_id)
                .ForeignKey("dbo.Buzon", t => t.Buzon_id)
                .ForeignKey("dbo.Calidad", t => t.Calidad_id)
                .Index(t => t.Calidad_id)
                .Index(t => t.Buzon_id);
            
            CreateTable(
                "dbo.Calidad",
                c => new
                    {
                        Calidad_id = c.Int(nullable: false, identity: true),
                        Calidad_marca = c.String(maxLength: 3, unicode: false),
                        Calidad_descripcion = c.String(maxLength: 255, unicode: false),
                    })
                .PrimaryKey(t => t.Calidad_id);
            
            CreateTable(
                "dbo.Escuadrias",
                c => new
                    {
                        Escuadrias_id = c.Int(nullable: false, identity: true),
                        Escuadrias_nombre = c.String(),
                        Calidad_id = c.Int(),
                        Escuadrias_espesor = c.Double(),
                        Escuadrias_ancho = c.Double(),
                        Escuadrias_largo = c.Double(),
                        Escuadrias_porcentaje_participacion = c.Int(),
                        Escuadrias_porcentaje_espesor = c.Int(),
                        Escuadrias_porcentaje_ancho = c.Int(),
                        Escuadrias_porcentaje_largo = c.Int(),
                        Escuadrias_cantidad_tablas = c.Int(),
                    })
                .PrimaryKey(t => t.Escuadrias_id)
                .ForeignKey("dbo.Calidad", t => t.Calidad_id)
                .Index(t => t.Calidad_id);
            
            CreateTable(
                "dbo.Tabla",
                c => new
                    {
                        Tabla_id = c.Int(nullable: false, identity: true),
                        Calidad_id = c.Int(),
                        Tabla_espesor = c.Double(),
                        Tabla_ancho = c.Double(),
                        Tabla_largo_inicial = c.Double(),
                        Tabla_largo_final = c.Double(),
                        Tabla_fecha_hora = c.DateTime(),
                    })
                .PrimaryKey(t => t.Tabla_id)
                .ForeignKey("dbo.Calidad", t => t.Calidad_id)
                .Index(t => t.Calidad_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tabla", "Calidad_id", "dbo.Calidad");
            DropForeignKey("dbo.Escuadrias", "Calidad_id", "dbo.Calidad");
            DropForeignKey("dbo.Buzon", "Escuadrias_id", "dbo.Escuadrias");
            DropForeignKey("dbo.Carro", "Calidad_id", "dbo.Calidad");
            DropForeignKey("dbo.Carro", "Buzon_id", "dbo.Buzon");
            DropIndex("dbo.Tabla", new[] { "Calidad_id" });
            DropIndex("dbo.Escuadrias", new[] { "Calidad_id" });
            DropIndex("dbo.Carro", new[] { "Buzon_id" });
            DropIndex("dbo.Carro", new[] { "Calidad_id" });
            DropIndex("dbo.Buzon", new[] { "Escuadrias_id" });
            DropTable("dbo.Tabla");
            DropTable("dbo.Escuadrias");
            DropTable("dbo.Calidad");
            DropTable("dbo.Carro");
            DropTable("dbo.Buzon");
        }
    }
}
