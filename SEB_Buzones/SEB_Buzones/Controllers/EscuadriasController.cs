﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SEB_Buzones.Models;

namespace SEB_Buzones.Controllers
{
    public class EscuadriasController : Controller
    {
        private Seb_BuzonesModel db = new Seb_BuzonesModel();

        // GET: Escuadrias
        public ActionResult Index()
        {
            var escuadrias = db.Escuadrias.Include(e => e.Calidad);
            return View(escuadrias.ToList());
        }

        public JsonResult List()
        {
            var lista = db.ListAll();
           
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Add(Escuadria escuadria)
        {
            return Json(db.Add(escuadria), JsonRequestBehavior.AllowGet);
        }

        public JsonResult ListEscuadrias()
        {
            var lista = db.ListEscuadrias();
           
            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetbyID(int ID)
        {
            var Escuadria = db.ListAll().Find(x => x.Escuadrias_id.Equals(ID));
            return Json(Escuadria, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Update(Escuadria emp)
        {
            var update = db.Update(emp);
            return Json(update, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Delete(int ID)
        {
            return Json(db.Delete(ID), JsonRequestBehavior.AllowGet);
        }

    }
}
