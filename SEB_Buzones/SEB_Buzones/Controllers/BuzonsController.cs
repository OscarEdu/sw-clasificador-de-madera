﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using SEB_Buzones.Models;

namespace SEB_Buzones.Controllers
{
    public class BuzonsController : Controller
    {
        private Seb_BuzonesModel db = new Seb_BuzonesModel();

        // GET: Buzons
        public ActionResult Index()
        {
            var buzons = db.Buzons.Include(b => b.Escuadria);
            return View(buzons.ToList());
        }

        public JsonResult List()
        {
          var lista = db.ListBuzons();

          return Json(lista, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult Update(List<Buzon> buzon)
        {
            var update = db.UpdateBuzon(buzon);
            return Json(update, JsonRequestBehavior.AllowGet);
        }

        // GET: Buzons/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buzon buzon = db.Buzons.Find(id);
            if (buzon == null)
            {
                return HttpNotFound();
            }
            return View(buzon);
        }

        // GET: Buzons/Create
        public ActionResult Create()
        {
            ViewBag.Escuadrias_id = new SelectList(db.Escuadrias, "Escuadrias_id", "Escuadrias_id");
            return View();
        }

        // POST: Buzons/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Buzon_id,Buzon_cantidad,Buzon_total,Buzon_estado,Escuadrias_id")] Buzon buzon)
        {
            if (ModelState.IsValid)
            {
                db.Buzons.Add(buzon);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Escuadrias_id = new SelectList(db.Escuadrias, "Escuadrias_id", "Escuadrias_id", buzon.Escuadrias_id);
            return View(buzon);
        }

        // GET: Buzons/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buzon buzon = db.Buzons.Find(id);
            if (buzon == null)
            {
                return HttpNotFound();
            }
            ViewBag.Escuadrias_id = new SelectList(db.Escuadrias, "Escuadrias_id", "Escuadrias_id", buzon.Escuadrias_id);
            return View(buzon);
        }

        // POST: Buzons/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Buzon_id,Buzon_cantidad,Buzon_total,Buzon_estado,Escuadrias_id")] Buzon buzon)
        {
            if (ModelState.IsValid)
            {
                db.Entry(buzon).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Escuadrias_id = new SelectList(db.Escuadrias, "Escuadrias_id", "Escuadrias_id", buzon.Escuadrias_id);
            return View(buzon);
        }

        // GET: Buzons/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Buzon buzon = db.Buzons.Find(id);
            if (buzon == null)
            {
                return HttpNotFound();
            }
            return View(buzon);
        }

        // POST: Buzons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Buzon buzon = db.Buzons.Find(id);
            db.Buzons.Remove(buzon);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



        public ActionResult Asignar()
        {
            var buzons = db.Buzons.Include(b => b.Escuadria);
            return View(buzons.ToList());
        }



    }




}
